var backdrop = document.querySelector('.backdrop');
var modal = document.querySelector('.modal');
var noButton = document.querySelector('.modal__action--negative')
var selectPlanButtons = document.querySelectorAll('.plan button');
var toggleButton = document.querySelector('.toggle-button');
var mobileNav = document.querySelector('.mobile-nav');

// console.dir(selectPlanButtons);

for (var i = 0; i < selectPlanButtons.length; i++) {
    selectPlanButtons[i].addEventListener('click', function () {
        // modal.style.display = 'block';
        // backdrop.style.display = 'block';
        // modal.className = 'open'; //This will actually overwrite the complete class list.
        modal.classList.add('open');
        backdrop.classList.add('open');
    })
}


backdrop.addEventListener('click', function () {
    // modal.style.display = 'none';
    // backdrop.style.display = 'none';
    // mobileNav.style.display = 'none';
    if(modal) {
        modal.classList.remove('open');
    }
    backdrop.classList.remove('open');
    mobileNav.classList.remove('open');
});

if (noButton) {
    noButton.addEventListener('click', function () {
        // modal.style.display = 'none';
        // backdrop.style.display = 'none';
        if(modal) {
            modal.classList.remove('open');
        }
        backdrop.classList.remove('open');
    });
};

toggleButton.addEventListener('click', function() {
    mobileNav.classList.add('open');
    backdrop.classList.add('open');
});